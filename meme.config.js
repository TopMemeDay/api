const env = {
  NODE_ENV: 'production',
};
module.exports = {
  apps: [
    {
      name: 'meme-api',
      script: 'npm',
      args: ['start', 'prod'],
      watch: false,
      instances: 1,
      env,
      exec_mode: 'fork',
      time: true,
    },
  ],
};
