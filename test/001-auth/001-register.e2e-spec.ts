import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AuthModule } from './../../src/auth/auth.module';
import { UserModule } from './../../src/user/user.module';

describe('AuthController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AuthModule, UserModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/register (POST)', () => {
    const registerPayload = {
      email: 'test@email.com',
      password: '11111111',
      firstName: 'Vitaliy',
      lastName: 'Hryhoriv',
      nickname: 'vitaki',
      gender: 'male',
    };
    return request(app.getHttpServer())
      .post('/register')
      .send(JSON.stringify(registerPayload))
      .expect(201)
      .expect({ success: true });
  });
});
