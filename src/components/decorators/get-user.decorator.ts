import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IUserReadable } from 'src/user/interfaces/readableUser.interface';

export const GetUser = createParamDecorator(
  (data: unknown, ctx: ExecutionContext): IUserReadable => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);
