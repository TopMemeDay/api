import { ApiProperty } from '@nestjs/swagger';

export class ResponseDto {
  @ApiProperty()
  readonly success: boolean;
}
