import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { UploadModule } from './upload/upload.module';
import { configModule } from './config.root';
import { mongooseModule } from './mogoose.root';
import { LoggerModule } from './logger/logger.module';
import { PostModule } from './post/post.module';
import { CommentModule } from './comment/comment.module';

@Module({
  imports: [
    UserModule,
    AuthModule,
    configModule,
    mongooseModule,
    UploadModule,
    LoggerModule,
    PostModule,
    CommentModule,
  ],
})
export class AppModule {}
