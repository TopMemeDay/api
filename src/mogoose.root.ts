import { MongooseModule } from '@nestjs/mongoose';

export const mongooseModule = MongooseModule.forRoot(process.env.DB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});
