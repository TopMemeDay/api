import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { Logger, ValidationPipe } from '@nestjs/common';
import ip from 'ip';
import * as helmet from 'helmet';
import * as fs from 'fs';
import { ResponseInterceptor } from './components/interceptors/response.interceptor';
import { HttpExceptionFilter } from './components/filters/exeption.filter';
import { ConfigService } from '@nestjs/config';
import { LoggerService } from './logger/logger.service';

async function bootstrap() {
  const keyFile = fs.readFileSync(process.env.SSL_OUTKEY);
  const certFile = fs.readFileSync(process.env.SSL_OUTCRT);

  const app = await NestFactory.create(AppModule, {
    logger: new Logger(),
    httpsOptions: {
      key: keyFile,
      cert: certFile,
    },
  });

  const configService = app.get(ConfigService);
  let logger;
  try {
    logger = await app.resolve(LoggerService);
  } catch (e) {
    const error = 'Failed to init logger';
    throw error;
  }

  // ENV VARIABLES
  const env = configService.get<string>('NODE_ENV');
  const port = configService.get<number>('SERVER_PORT');
  const name = configService.get<string>('SERVER_NAME');
  const mount = configService.get<string>('SERVER_MOUNT');

  if (env === 'development') {
    const origin = [`http://localhost:${port}`, `http://127.0.0.1:${port}`];

    const lanIP = ip.address();
    if (lanIP) {
      origin.push(`http://${lanIP}:${port}`);
    }
    app.enableCors({ origin, credentials: true });
  }

  app.setGlobalPrefix(mount);
  app.useGlobalInterceptors(new ResponseInterceptor());
  app.useGlobalFilters(new HttpExceptionFilter());
  app.use(helmet());
  app.useGlobalPipes(
    new ValidationPipe({ forbidNonWhitelisted: true, whitelist: true }),
  );

  // OPENAPI CONFIGURATION
  const config = new DocumentBuilder()
    .setTitle('Top Meme Day API')
    .setDescription('Doc about meme api and all needed info')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, config, {
    ignoreGlobalPrefix: true,
    deepScanRoutes: true,
  });
  SwaggerModule.setup(`${mount}/doc`, app, document);

  await app.listen(port, () => {
    logger.log(`${name} is running on port: ${port}`, 'SERVER');
  });
}
bootstrap();
