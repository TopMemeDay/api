import {
  Controller,
  UseGuards,
  Post,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/components/decorators/get-user.decorator';
import { IUserReadable } from 'src/user/interfaces/readableUser.interface';
import { JwtAuthGuard } from './../auth/guards/jwt.guard';
import { UploadDto } from './dto/upload.dto';
import { IUploadReadable } from './interfaces/readableUpload.interface';
import { UploadService } from './upload.service';

@ApiTags('uploads')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('uploads')
export class UploadController {
  constructor(private readonly UploadService: UploadService) {}

  @UseInterceptors(FileInterceptor('image'))
  @Post()
  async uploadImage(
    @UploadedFile() file: UploadDto,
    @GetUser() user: IUserReadable,
  ): Promise<{ upload: IUploadReadable }> {
    return await this.UploadService.uploadImage(file, user);
  }
}
