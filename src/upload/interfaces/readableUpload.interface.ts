import { User } from 'src/user/schemas/user.schema';
import { Upload } from '../schemas/upload.schema';

export interface IUploadReadable {
  readonly id?: Upload;
  readonly userId: User;
  readonly size: number;
  readonly location: string;
  readonly mimetype: string;
  readonly dimensions: string;
  readonly extension: string;
}
