import { Document } from 'mongoose';
import { User } from 'src/user/schemas/user.schema';
import { Upload } from '../schemas/upload.schema';

export interface IUpload extends Document {
  readonly _id?: Upload;
  readonly id?: string;
  readonly userId: User;
  readonly size: number;
  readonly filename: string;
  readonly location: string;
  readonly mimetype: string;
  readonly dimensions: string;
  readonly extension: string;
  readonly canDelete: boolean;
  readonly deletedFromDisk: boolean;
  readonly lastDeleteAttemptAt: Date;
  readonly deleteAttemptsCount: number;
  readonly del: boolean;
  readonly deletedAt: Date;
  readonly updatedAt?: Date;
  readonly createdAt?: Date;
}
