export enum MimeTypes {
  'image/jpeg',
  'image/png',
  'image/svg+xml',
  'image/gif',
  // 'video/webm',
}
