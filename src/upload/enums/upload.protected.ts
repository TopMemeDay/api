export enum UploadProtectedEnum {
  '_ID' = '_id',
  'FILENAME' = 'filename',
  'CANDELETE' = 'canDelete',
  'DELETEDFROMDISK' = 'deletedFromDisk',
  'LASTDELETEATTEMPTAT' = 'lastDeleteAttemptAt',
  'DELETEATTEMPTSCOUNT' = 'deleteAttemptsCount',
  'DEL' = 'del',
  'DELETEDAT' = 'deletedAt',
  '__V' = '__v',
  'CREATEDAT' = 'createdAt',
  'UPDATEDAT' = 'updatedAt',
}
