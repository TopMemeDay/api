import { Module } from '@nestjs/common';
import { UploadService } from './upload.service';
import { UploadController } from './upload.controller';
import { LoggerModule } from 'src/logger/logger.module';
import { ConfigModule } from '@nestjs/config';
import { Upload, UploadSchema } from './schemas/upload.schema';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  providers: [UploadService],
  controllers: [UploadController],
  imports: [
    LoggerModule,
    ConfigModule,
    MongooseModule.forFeature([{ name: Upload.name, schema: UploadSchema }]),
    LoggerModule,
  ],
})
export class UploadModule {}
