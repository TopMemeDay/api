import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import * as _ from 'lodash';
import * as axios from 'axios';
import imageSize from 'image-size';
import { Model } from 'mongoose';
import { LoggerService } from 'src/logger/logger.service';
import { MimeTypes } from './enums/mimetype.enum';
import { Upload, UploadDocument } from './schemas/upload.schema';
import { IUserReadable } from 'src/user/interfaces/readableUser.interface';
import { IUploadReadable } from './interfaces/readableUpload.interface';
import { UploadProtectedEnum } from './enums/upload.protected';
import { IUpload } from './interfaces/upload.interface';
import { User } from 'src/user/schemas/user.schema';

@Injectable()
export class UploadService {
  constructor(
    private readonly logger: LoggerService,
    private readonly ConfigService: ConfigService,
    @InjectModel(Upload.name) private uploadModel: Model<UploadDocument>,
  ) {
    this.logger.setContext('upload');
  }

  checkMimeType(image: Express.Multer.File): boolean {
    return Object.keys(MimeTypes).includes(image.mimetype);
  }

  async makeRequest(url, method, data): Promise<axios.AxiosResponse> {
    let response;
    try {
      response = await axios.default.request({ url, method, data });
    } catch (err) {
      const error = 'Failed to make request';
      this.logger.error(error, err);
    }
    return response;
  }

  async saveToDB(
    image: Express.Multer.File,
    location: string,
    userId: User,
  ): Promise<Upload> {
    const dataPayload = {
      userId,
      size: image.size,
      filename: image.originalname,
      location,
      mimetype: image.mimetype,
      dimensions: '',
      extension: image.mimetype.split('/')[1],
    };

    try {
      const height = imageSize(image.buffer).height;
      const width = imageSize(image.buffer).width;
      dataPayload.dimensions = `${width}x${height}`;
    } catch (e) {
      const error = `Failed to get dimensions from file, ${e}`;
      this.logger.error(error);
    }

    let upload;
    try {
      upload = new this.uploadModel(dataPayload);
    } catch (err) {
      const error = 'Failed to create db document';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    try {
      await upload.save();
    } catch (err) {
      const error = 'Failed to save document in db';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    return upload;
  }

  async uploadImage(
    image: Express.Multer.File,
    user: IUserReadable,
  ): Promise<{ upload: IUploadReadable }> {
    const correctMime = this.checkMimeType(image);

    if (!correctMime) {
      const error = 'Invalid mime type';
      this.logger.error(error);
      throw new BadRequestException(null, error);
    }

    const thumborUrl = this.ConfigService.get<string>('THUMBOR_URL');
    const thumborMethod = this.ConfigService.get<string>('THUMBOR_METHOD');

    const response = await this.makeRequest(
      thumborUrl,
      thumborMethod,
      image.buffer,
    );

    if (response.status !== 201 || !_.has(response.headers, 'location')) {
      const error = 'Failed to save upload';
      this.logger.error(error);
    }

    const location = _.get(response.headers, 'location', null);

    let upload;
    try {
      upload = await this.saveToDB(image, location, user.id);
    } catch (err) {
      const error = 'Failed to save upload to db';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    const saveUpload = this.getProtectedUpload(upload);
    return { upload: saveUpload };
  }

  getProtectedUpload(upload: IUpload): IUploadReadable {
    const uploadObj = (upload.toObject() as unknown) as IUploadReadable;
    return _.omit<any>(
      uploadObj,
      ...Object.values(UploadProtectedEnum),
    ) as IUploadReadable;
  }

  async findManyByFilter(filter): Promise<UploadDocument[]> {
    return await this.uploadModel.find(filter);
  }
}
