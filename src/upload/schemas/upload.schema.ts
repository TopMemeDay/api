import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from '../../user/schemas/user.schema';

export type UploadDocument = Upload & mongoose.Document;

@Schema({ timestamps: true, id: true, versionKey: false })
export class Upload {
  @Prop({
    index: true,
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
  })
  userId: User;

  @Prop({
    default: 0,
  })
  size: number;

  @Prop({
    default: '',
  })
  filename: string;

  @Prop({
    default: '',
  })
  location: string;

  @Prop({
    default: '',
  })
  mimetype: string;

  @Prop({
    default: '',
  })
  dimensions: string;

  @Prop({
    default: '',
  })
  extension: string;

  @Prop({ default: false, index: true, select: false })
  canDelete: boolean;

  @Prop({ default: false, index: true, select: false })
  deletedFromDisk: boolean;

  @Prop({ default: null, select: false })
  lastDeleteAttemptAt: Date;

  @Prop({ default: 0, select: false })
  deleteAttemptsCount: number;

  @Prop({ select: false, default: false })
  del: boolean;
  @Prop({ default: null, select: false })
  deletedAt: Date;
}

export const UploadSchema = SchemaFactory.createForClass(Upload);

UploadSchema.set('toObject', { virtuals: true, flattenMaps: true });
UploadSchema.set('toJSON', { virtuals: true, flattenMaps: true });
