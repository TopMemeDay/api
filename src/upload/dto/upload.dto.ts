import { ApiConsumes } from '@nestjs/swagger';

@ApiConsumes('multipart/form-data')
export class UploadDto implements Express.Multer.File {
  readonly mimetype: string;
  readonly originalname: string;
  readonly path: string;
  readonly size;
  readonly stream;
  readonly destination;
  readonly fieldname;
  readonly filename;
  readonly buffer;
  readonly encoding;
}
