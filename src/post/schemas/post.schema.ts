import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Upload } from '../../upload/schemas/upload.schema';
import { User } from '../../user/schemas/user.schema';

export type PostDocument = Post & mongoose.Document;
@Schema({ timestamps: true, id: true, versionKey: false })
export class Post {
  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    index: true,
    trim: true,
    ref: 'User',
  })
  authorId: User;

  @Prop({
    type: [
      {
        ref: 'Upload',
        type: mongoose.Schema.Types.ObjectId,
        max: 5,
        unique: true,
      },
    ],
    required: true,
    default: [],
  })
  content: Upload[];

  @Prop({
    text: true,
    trim: true,
    type: String,
    default: null,
    maxlength: 4000,
  })
  description: string;

  @Prop({
    index: true,
    default: [],
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        unique: true,
      },
    ],
  })
  likes: User[];

  @Prop({ index: true, default: 0, type: Number })
  likesCount: number;

  @Prop({ type: Boolean, select: false, default: false })
  del: boolean;

  @Prop({ type: Date, select: false, default: null })
  deletedAt: Date;
}

export const PostSchema = SchemaFactory.createForClass(Post);

PostSchema.set('toObject', { virtuals: true, flattenMaps: true });
PostSchema.set('toJSON', { virtuals: true, flattenMaps: true });
