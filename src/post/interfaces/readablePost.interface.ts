import { Upload } from 'src/upload/schemas/upload.schema';
import { User } from 'src/user/schemas/user.schema';
import { Post } from '../schemas/post.schema';

export interface IPostReadable {
  readonly id?: Post;
  readonly authorId: User;
  readonly content: Upload[];
  readonly description: string;
  readonly likes: User[];
  readonly likesCount: number;
}
