import { Document } from 'mongoose';
import { Upload } from 'src/upload/schemas/upload.schema';
import { User } from 'src/user/schemas/user.schema';
import { Post } from '../schemas/post.schema';

export interface IPost extends Document {
  readonly _id?: Post;
  readonly id?: string;
  readonly authorId: User;
  readonly content: Upload[];
  readonly description: string;
  readonly likes: User[];
  readonly likesCount: number;
  readonly del: boolean;
  readonly deletedAt: Date;
  readonly updatedAt?: Date;
  readonly createdAt?: Date;
}
