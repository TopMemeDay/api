import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guards/jwt.guard';
import { GetUser } from 'src/components/decorators/get-user.decorator';
import { IUser } from 'src/user/interfaces/user.interface';
import { CreatePostDto } from './dto/create-post.dto';
import { PopularPostsDto } from './dto/popular-posts.dto';
import { UsersPostDto } from './dto/users-post.dto';
import { IPost } from './interfaces/post.interface';
import { IPostReadable } from './interfaces/readablePost.interface';
import { PostService } from './post.service';

@ApiTags('posts')
@Controller('posts')
export class PostController {
  constructor(private readonly PostService: PostService) {}

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  async createPost(
    @Body() CreatePostDto: CreatePostDto,
    @GetUser() user: IUser,
  ): Promise<{ post: IPostReadable }> {
    return await this.PostService.create(CreatePostDto, user._id);
  }

  @Get('popular')
  async getPopular(
    @Query() popularDto: PopularPostsDto,
  ): Promise<{ posts: IPostReadable[] }> {
    return await this.PostService.getPopular(popularDto);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put('like/:id')
  async like(@Param('id') id: IPost, @GetUser() user: IUser): Promise<void> {
    return await this.PostService.like(id, user);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete('like/:id')
  async dislike(@Param('id') id: IPost, @GetUser() user: IUser): Promise<void> {
    return await this.PostService.dislike(id, user);
  }

  @Get('users')
  async getUsersPosts(
    @Query() usersPostDto: UsersPostDto,
  ): Promise<{ posts: IPostReadable[] }> {
    return await this.PostService.getByUserId(usersPostDto);
  }

  @Get(':id')
  async getUserPosts(
    @Param('id') id: string,
  ): Promise<{ post: IPostReadable }> {
    return await this.PostService.findOne(id);
  }
}
