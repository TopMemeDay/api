import {
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { Model } from 'mongoose';
import * as _ from 'lodash';
import { InjectModel } from '@nestjs/mongoose';
import * as moment from 'moment';
import { LoggerService } from 'src/logger/logger.service';
import { IPost } from './interfaces/post.interface';
import { IPostReadable } from './interfaces/readablePost.interface';
import { Post, PostDocument } from './schemas/post.schema';
import { PostProtectedEnum } from './enums/post.protected';
import { CreatePostDto } from './dto/create-post.dto';
import { PopularPostsDto } from './dto/popular-posts.dto';
import { UsersPostDto } from './dto/users-post.dto';
import { User } from 'src/user/schemas/user.schema';
import { IUser } from 'src/user/interfaces/user.interface';

@Injectable()
export class PostService {
  constructor(
    @InjectModel(Post.name) private postModel: Model<PostDocument>,
    private readonly logger: LoggerService,
  ) {
    this.logger.setContext('posts');
  }

  async create(
    createPostDto: CreatePostDto,
    userId: User,
  ): Promise<{ post: IPostReadable }> {
    const post = new this.postModel(createPostDto);
    post.authorId = userId;

    try {
      await post.save();
    } catch (err) {
      const error = 'Failed to save post';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    const readablePost = this.getReadable(post);

    this.logger.log(`Post created: ${post.id}`);

    return { post: readablePost };
  }

  getReadable(post: IPost): IPostReadable {
    const postObj = (post.toObject() as unknown) as IPostReadable;
    return _.omit<any>(
      postObj,
      ...Object.values(PostProtectedEnum),
    ) as IPostReadable;
  }

  async findOne(id: string): Promise<{ post: IPostReadable }> {
    let post;
    try {
      post = await this.postModel.findById(id).populate('content', 'location');
    } catch (err) {
      const error = 'Failed to get post by id';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    if (!post) {
      return;
    }

    return { post: this.getReadable(post) };
  }

  getTimestampByText(text: string): Date {
    const txt = text as moment.unitOfTime.DurationConstructor;
    return moment(new Date()).subtract(1, txt).toDate();
  }

  async getPopular(
    popularDto: PopularPostsDto,
  ): Promise<{ posts: IPostReadable[] }> {
    const timestamp = this.getTimestampByText(popularDto.per);

    let filter = {
      createdAt: { $gte: timestamp },
    };

    if (popularDto.per === 'all') {
      filter = undefined;
    }

    let posts;
    try {
      posts = await this.postModel
        .find(filter)
        .populate('content', 'location')
        .skip(Number(popularDto.skip))
        .limit(Number(popularDto.limit))
        .sort({ likesCount: 'desc' });
    } catch (err) {
      const error = 'Failed to find posts';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    const savePosts = _.map(posts, (post) => this.getReadable(post));

    return { posts: savePosts };
  }

  async like(_id: Post, user: IUser): Promise<void> {
    let post;
    try {
      post = await this.postModel.findOneAndUpdate(
        {
          _id,
          del: false,
          likes: {
            $nin: [user._id],
          },
        },
        {
          $addToSet: {
            likes: {
              $each: [user._id],
            },
          },
          $inc: {
            likesCount: 1,
          },
        },
      );
    } catch (err) {
      const error = 'Failed to update post';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    if (!post) {
      const error = 'Liked post not found';
      this.logger.error(error);
      throw new NotFoundException(null, error);
    }

    this.logger.log(`${user._id} liked post ${_id}`);
  }

  async dislike(_id: Post, user: IUser): Promise<void> {
    let post;
    try {
      post = await this.postModel.findOneAndUpdate(
        {
          _id,
          del: false,
          likes: {
            $in: [user._id],
          },
        },
        {
          $pull: {
            likes: user._id,
          },
          $inc: {
            likesCount: -1,
          },
        },
      );
    } catch (err) {
      const error = 'Failed to update post';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    if (!post) {
      const error = 'Liked post not found';
      this.logger.error(error);
      throw new NotFoundException(null, error);
    }
    this.logger.log(`${user._id} unliked post ${_id}`);
  }

  async getByUserId(
    usersPostDto: UsersPostDto,
  ): Promise<{ posts: IPostReadable[] }> {
    let posts;
    try {
      posts = await this.postModel
        .find({ authorId: usersPostDto.id, del: false })
        .populate('content', 'location')
        .skip(Number(usersPostDto.skip))
        .limit(Number(usersPostDto.limit))
        .sort({ createdAt: 'desc' });
    } catch (err) {
      const error = 'Failed to get posts';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    const safePosts = _.map(posts, (post) => this.getReadable(post));

    return { posts: safePosts };
  }
}
