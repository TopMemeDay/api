import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsEnum,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { timeCategoriesEnum } from '../enums/time-categories.enum';

export class PopularPostsDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @IsEnum(timeCategoriesEnum)
  readonly per: string;
  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsNumberString()
  readonly skip: number;
  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsNumberString()
  readonly limit: number;
}
