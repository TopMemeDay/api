import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  ArrayUnique,
  IsArray,
  IsNotEmpty,
  IsOptional,
  IsString,
  MaxLength,
} from 'class-validator';
import { Upload } from 'src/upload/schemas/upload.schema';

export class CreatePostDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  @ArrayUnique()
  readonly content: Upload[];

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  @MaxLength(2000)
  readonly description: string;
}
