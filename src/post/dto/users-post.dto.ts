import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import {
  IsMongoId,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';
import { User } from 'src/user/schemas/user.schema';

export class UsersPostDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @IsMongoId()
  readonly id: User;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumberString()
  @IsNotEmpty()
  readonly skip: number;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNumberString()
  @IsNotEmpty()
  readonly limit: number;
}
