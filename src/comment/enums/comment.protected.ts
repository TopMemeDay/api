export enum commentProtectedEnum {
  '_ID' = '_id',
  '__V' = '__v',
  'DEL' = 'del',
  'DELETEDAT' = 'deletedAt',
  'CREATEDAT' = 'createdAt',
  'UPDATEDAT' = 'updatedAt',
}
