import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Post } from 'src/post/schemas/post.schema';
import { User } from 'src/user/schemas/user.schema';

export type CommentDocument = Comment & mongoose.Document;

@Schema({ timestamps: true, id: true, versionKey: false })
export class Comment {
  @Prop({
    index: true,
    required: true,
    ref: 'User',
    type: mongoose.Schema.Types.ObjectId,
  })
  userId: User;

  @Prop({
    index: true,
    required: true,
    ref: 'Post',
    type: mongoose.Schema.Types.ObjectId,
  })
  postId: Post;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Comment',
    index: true,
  })
  parrentId: Comment;

  @Prop({
    trim: true,
    text: true,
    maxlength: 2000,
  })
  text: string;

  @Prop({ select: false, default: false })
  del: boolean;
  @Prop({ default: null, select: false })
  deletedAt: Date;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);

CommentSchema.set('toObject', { virtuals: true, flattenMaps: true });
CommentSchema.set('toJSON', { virtuals: true, flattenMaps: true });
