import { Post } from 'src/post/schemas/post.schema';
import { User } from 'src/user/schemas/user.schema';
import { Comment } from '../schemas/comment.schema';

export interface ICommentReadable {
  readonly id: Comment;
  readonly userId: User;
  readonly postId: Post;
  readonly parrentId: Comment;
  readonly text: string;
}
