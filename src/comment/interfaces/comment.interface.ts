import { Document } from 'mongoose';
import { Post } from 'src/post/schemas/post.schema';
import { User } from 'src/user/schemas/user.schema';
import { Comment } from '../schemas/comment.schema';

export interface IComment extends Document {
  readonly _id?: Comment;
  readonly id?: string;
  readonly userId: User;
  readonly postId: Post;
  readonly parrentId: Comment;
  readonly text: string;
  readonly del: boolean;
  readonly deletedAt: Date;
  readonly updatedAt?: Date;
  readonly createdAt?: Date;
}
