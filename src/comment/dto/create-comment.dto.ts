import { ApiProperty, ApiPropertyOptional } from '@nestjs/swagger';
import { IsMongoId, IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Post } from 'src/post/schemas/post.schema';
import { User } from 'src/user/schemas/user.schema';

export class CreateCommentDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  readonly userId: User;

  @ApiProperty()
  @IsNotEmpty()
  @IsMongoId()
  readonly postId: Post;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly text: string;

  @ApiPropertyOptional()
  @IsOptional()
  @IsNotEmpty()
  @IsMongoId()
  parrentId: User;
}
