import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import * as bcrypt from 'bcrypt';
import * as _ from 'lodash';

import { CreateUserDto } from './dto/create-user.dto';
import { IUser } from './interfaces/user.interface';
import { User, UserDocument } from './schemas/user.schema';
import { IUserReadable } from './interfaces/readableUser.interface';
import { userProtectedEnum } from './enums/user.protected';
import { UpdateUserDto } from './dto/update-user.dto';
import { LoggerService } from '../logger/logger.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class UserService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    private readonly logger: LoggerService,
    private readonly ConfigService: ConfigService,
  ) {
    this.logger.setContext('users');
  }

  async create(createUserDto: CreateUserDto): Promise<IUserReadable> {
    const saltRounds = this.ConfigService.get<number>('SALT_ROUNDS');

    let salt;
    try {
      salt = await bcrypt.genSalt(+saltRounds);
    } catch (err) {
      const error = 'Failed to hash password';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    let hashedPassword;
    try {
      hashedPassword = await bcrypt.hash(createUserDto.password, salt);
    } catch (err) {
      const error = "Failed to hash user's password";
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    const user = new this.userModel(
      _.assignIn(createUserDto, { password: hashedPassword }),
    );

    try {
      await user.save();
    } catch (err) {
      const error = 'Failed to save new user';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    this.logger.log(`Created new user: ${user.id}`);
    return this.getReadableUser(user);
  }

  async findById(
    id: IUser,
    ownUser: IUserReadable,
  ): Promise<{ user: IUserReadable }> {
    if (!ownUser && !id) {
      const error = 'No user id set';
      this.logger.error(error);
      throw new BadRequestException(null, error);
    }

    const userIdStr = id || ownUser.id;

    let userId;
    try {
      userId = Types.ObjectId(String(userIdStr));
    } catch (err) {
      const error = 'Failed to validate id';
      this.logger.error(error, err);
      throw new BadRequestException(null, error);
    }

    let user;
    try {
      user = await this.userModel
        .findById(userId)
        .populate('avatarImage', 'location');
    } catch (err) {
      const error = 'Failed to save new user';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    if (!user) {
      const error = 'User not found';
      this.logger.error(error);
      throw new NotFoundException(null, error);
    }

    user = this.getReadableUser(user);
    return { user };
  }

  async countDocuments(filter): Promise<number> {
    let count;
    try {
      count = await this.userModel.countDocuments(filter);
    } catch (err) {
      const error = 'Failed to count documents';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }
    return count;
  }

  async findOneByFilter(filter): Promise<IUser> {
    let user;
    try {
      user = await this.userModel.findOne(filter);
    } catch (err) {
      const error = 'Failed to find user';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }
    return user;
  }

  async comparePass(pass: string, hash: string): Promise<boolean> {
    let result;
    try {
      result = await bcrypt.compare(pass, hash);
    } catch (err) {
      const error = 'Failed compare passwords';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }
    return result;
  }

  getReadableUser(user: IUser): IUserReadable {
    const userObj = (user.toObject() as unknown) as IUserReadable;
    return _.omit<any>(
      userObj,
      ...Object.values(userProtectedEnum),
    ) as IUserReadable;
  }

  async update(
    user: UpdateUserDto,
    reqUser: IUserReadable,
    id: IUser,
  ): Promise<IUserReadable> {
    const userId = id || reqUser.id;

    if (String(userId) !== String(reqUser.id)) {
      const error = 'User not found';
      this.logger.error(error);
      throw new BadRequestException(null, error);
    }

    let updatedUser;
    try {
      updatedUser = await this.userModel.findOneAndUpdate(
        { _id: userId, del: false },
        {
          $set: { ...user },
        },
      );
    } catch (e) {
      const error = 'Failed to update user';
      this.logger.error(error, e);
      throw new InternalServerErrorException(null, error);
    }

    this.logger.log(`Updated user: ${userId}`);

    return this.getReadableUser(updatedUser);
  }

  async delete(user: IUserReadable, id: IUser): Promise<void> {
    const userId = id || user.id;

    if (String(userId) !== String(user.id)) {
      const error = 'User not found';
      this.logger.error(error);
      throw new NotFoundException(null, error);
    }
    try {
      await this.userModel.findByIdAndUpdate(userId, {
        $set: { del: true, deletedAt: new Date() },
      });
    } catch (e) {
      const error = 'Failed to delete user';
      this.logger.error(error, e);
      throw new InternalServerErrorException(null, error);
    }
    this.logger.log(`Deleted new user: ${userId}`);
  }

  async checkNickname(nickname: string): Promise<{ exist: boolean }> {
    let exist;
    try {
      exist = !!(await this.userModel.countDocuments({ nickname }));
    } catch (err) {
      const error = 'Failed to find nickname';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    return { exist };
  }

  async follow(id: User, user: IUserReadable): Promise<void> {
    if (String(id) === String(user.id)) {
      const error = "You can't follow yourself";
      this.logger.error(error);
      throw new BadRequestException(null, error);
    }

    try {
      await this.userModel.findOneAndUpdate(
        {
          _id: id,
          del: false,
          blockedByUsers: {
            $nin: [user.id],
          },
          blockedUsers: {
            $nin: [user.id],
          },
        },
        {
          $addToSet: {
            followers: user.id,
          },
        },
      );
    } catch (err) {
      const error = 'Failed to update user';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    try {
      await this.userModel.findOneAndUpdate(
        {
          _id: user.id,
          del: false,
          blockedByUsers: {
            $nin: [id],
          },
          blockedUsers: {
            $nin: [id],
          },
        },
        {
          $addToSet: {
            following: id,
          },
        },
      );
    } catch (err) {
      const error = 'Failed to update user';
      this.logger.error(error, err);
      throw new InternalServerErrorException(null, error);
    }

    this.logger.log(`user ${user.id} followed ${id}`);
  }
}
