export enum userProtectedEnum {
  '_ID' = '_id',
  'PASSWORD' = 'password',
  '__V' = '__v',
  'DEL' = 'del',
  'DELETEDAT' = 'deletedAt',
  'CREATEDAT' = 'createdAt',
  'UPDATEDAT' = 'updatedAt',
  'BLOCKEDUSERS' = 'blockedUsers',
  'BLOCKEDBYUSERS' = 'blockedByUsers',
  'ROLE' = 'role',
}
