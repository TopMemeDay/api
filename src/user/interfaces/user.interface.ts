import { Document } from 'mongoose';
import { Upload } from 'src/upload/schemas/upload.schema';
import { User } from 'src/user/schemas/user.schema';

export interface IUser extends Document {
  readonly _id?: User;
  readonly id?: string;
  readonly email: string;
  readonly password: string;
  readonly nickname: string;
  readonly avatarImage: Upload;
  readonly lang: string;
  readonly followers: User[];
  readonly following: User[];
  readonly blockedUsers: User[];
  readonly blockedByUsers: User[];
  readonly role: string;
  readonly del: boolean;
  readonly deletedAt: Date;
  readonly updatedAt?: Date;
  readonly createdAt?: Date;
}
