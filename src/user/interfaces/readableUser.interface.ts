import { Upload } from 'src/upload/schemas/upload.schema';
import { User } from 'src/user/schemas/user.schema';

export interface IUserReadable {
  readonly id?: User;
  readonly email: string;
  readonly nickname: string;
  readonly avatarImage: Upload;
  readonly followers: User[];
  readonly following: User[];
}
