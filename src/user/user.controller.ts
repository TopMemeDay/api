import {
  Controller,
  Get,
  Param,
  UseGuards,
  Body,
  Put,
  Delete,
  Post,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { GetUser } from 'src/components/decorators/get-user.decorator';
import { JwtAuthGuard } from './../auth/guards/jwt.guard';
import { UpdateUserDto } from './dto/update-user.dto';
import { IUserReadable } from './interfaces/readableUser.interface';
import { IUser } from './interfaces/user.interface';
import { User } from './schemas/user.schema';
import { UserService } from './user.service';

@ApiTags('users')
@Controller('users')
export class UserController {
  constructor(private readonly UserService: UserService) {}

  @Get('nicknameExist/:nickname')
  async checkNickname(
    @Param('nickname') nickname: string,
  ): Promise<{ exist: boolean }> {
    return await this.UserService.checkNickname(nickname);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get(':id*?')
  async find(
    @Param('id') id?: IUser,
    @GetUser() user?: IUserReadable,
  ): Promise<{ user: IUserReadable }> {
    return await this.UserService.findById(id, user);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Put(':id*?')
  async update(
    @Body() userDto: UpdateUserDto,
    @GetUser() reqUser: IUserReadable,
    @Param('id') id?: IUser,
  ): Promise<{ user: IUserReadable }> {
    const user = await this.UserService.update(userDto, reqUser, id);
    return { user };
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Delete(':id*?')
  async delete(
    @GetUser() user: IUserReadable,
    @Param('id') id?: IUser,
  ): Promise<void> {
    await this.UserService.delete(user, id);
  }

  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post('follow/:id')
  async follow(
    @Param('id') id: User,
    @GetUser() user: IUserReadable,
  ): Promise<void> {
    return await this.UserService.follow(id, user);
  }
}
