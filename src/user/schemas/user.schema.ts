import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Upload } from '../../upload/schemas/upload.schema';
import { Language } from '../enums/language.enum';
import { Roles } from '../enums/role.enum';

export type UserDocument = User & mongoose.Document;

@Schema({ timestamps: true, id: true, versionKey: false })
export class User {
  @Prop({
    index: true,
    required: true,
    unique: true,
  })
  email: string;

  @Prop({ required: true })
  password: string;

  @Prop({
    unique: true,
    index: true,
    required: true,
  })
  nickname: string;

  @Prop({
    ref: 'Upload',
    index: true,
    required: false,
    type: mongoose.Schema.Types.ObjectId,
  })
  avatarImage: Upload;

  @Prop({
    enum: Object.keys(Language),
    trim: true,
    default: 'en',
  })
  lang: string;

  @Prop({
    type: [
      {
        ref: 'User',
        type: mongoose.Schema.Types.ObjectId,
        index: true,
      },
    ],
    default: [],
  })
  followers: User[];

  @Prop({
    type: [
      {
        ref: 'User',
        type: mongoose.Schema.Types.ObjectId,
        index: true,
      },
    ],
    default: [],
  })
  following: User[];

  @Prop({
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        index: true,
      },
    ],
    default: [],
    index: true,
    select: false,
  })
  blockedUsers: User[];

  @Prop({
    type: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        index: true,
      },
    ],
    default: [],
  })
  blockedByUsers: User[];

  @Prop({
    trim: true,
    select: false,
    default: 'user',
    enum: Object.keys(Roles),
  })
  role: string;

  @Prop({ select: false, default: false })
  del: boolean;
  @Prop({ default: null, select: false })
  deletedAt: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.set('toObject', { virtuals: true, flattenMaps: true });
UserSchema.set('toJSON', { virtuals: true, flattenMaps: true });
