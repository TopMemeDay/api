import { IsEmail, IsString, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateUserDto {
  @ApiProperty({ example: 'test@email.com' })
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  readonly email: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly nickname: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  readonly password: string;
}
