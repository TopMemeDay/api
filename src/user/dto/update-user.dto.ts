import {
  IsString,
  IsNotEmpty,
  IsOptional,
  IsEnum,
  IsMongoId,
} from 'class-validator';
import { ApiPropertyOptional } from '@nestjs/swagger';
import { Upload } from 'src/upload/schemas/upload.schema';
import { Language } from '../enums/language.enum';

export class UpdateUserDto {
  @IsOptional()
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  readonly nickname: string;
  @IsOptional()
  @IsMongoId()
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  readonly avatarImage: Upload;
  @IsOptional()
  @ApiPropertyOptional()
  @IsNotEmpty()
  @IsString()
  @IsEnum(Language)
  readonly lang: string;
}
