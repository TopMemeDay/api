import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoggerService } from './../logger/logger.service';
import { CreateUserDto } from './../user/dto/create-user.dto';
import { IUserReadable } from './../user/interfaces/readableUser.interface';
import { UserService } from '../user/user.service';
import { LoginDto } from './dto/login.dto';

@Injectable()
export class AuthService {
  constructor(
    private readonly UserService: UserService,
    private readonly JwtService: JwtService,
    private readonly logger: LoggerService,
  ) {
    this.logger.setContext('auth');
  }

  async register(createUserDto: CreateUserDto): Promise<IUserReadable> {
    // check if registered email exist
    let emailExist;
    try {
      emailExist = await this.UserService.countDocuments({
        email: createUserDto.email,
        del: false,
      });
    } catch (err) {
      const error = 'Failed to count users';
      this.logger.error(err, error);
      throw new InternalServerErrorException(null, error);
    }

    if (emailExist > 0) {
      const error = 'Regisered user already exist';
      this.logger.error(error);
      throw new BadRequestException(null, error);
    }

    return await this.UserService.create(createUserDto);
  }

  async login(loginDto: LoginDto): Promise<{ token: string; id: string }> {
    const user = await this.UserService.findOneByFilter({
      email: loginDto.email,
      del: false,
    });

    if (!user) {
      const error = 'Invalid credentials';
      this.logger.error(error);
      throw new BadRequestException(null, error);
    }

    let passValid;
    try {
      passValid = await this.UserService.comparePass(
        loginDto.password,
        user.password,
      );
    } catch (e) {
      const error = 'Failed to find user';
      this.logger.error(error, e);
      throw new InternalServerErrorException(null, error);
    }

    if (!passValid) {
      const error = 'Invalid credentials';
      this.logger.error(error);
      throw new BadRequestException(null, error);
    }

    const safeUser = this.UserService.getReadableUser(user);
    const token = this.signToken(safeUser);
    return { token, id: String(user.id) };
  }

  signToken(payload: any): string {
    return this.JwtService.sign(payload);
  }
}
