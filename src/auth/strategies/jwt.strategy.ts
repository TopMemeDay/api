import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserService } from './../../user/user.service';
import { jwtConstants } from '../jwt.constants';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly UserService: UserService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(req: any) {
    const user = await this.UserService.findOneByFilter({
      _id: req.id,
      del: false,
    });

    if (user) {
      return this.UserService.getReadableUser(user);
    }
    return null;
  }
}
